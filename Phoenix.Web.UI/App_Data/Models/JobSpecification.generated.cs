//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.7.99
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Job Specification</summary>
	[PublishedContentModel("jobSpecification")]
	public partial class JobSpecification : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "jobSpecification";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public JobSpecification(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<JobSpecification, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Description
		///</summary>
		[ImplementPropertyType("description")]
		public IHtmlString Description
		{
			get { return this.GetPropertyValue<IHtmlString>("description"); }
		}

		///<summary>
		/// Email
		///</summary>
		[ImplementPropertyType("email")]
		public string Email
		{
			get { return this.GetPropertyValue<string>("email"); }
		}

		///<summary>
		/// Full Description
		///</summary>
		[ImplementPropertyType("fullDescription")]
		public string FullDescription
		{
			get { return this.GetPropertyValue<string>("fullDescription"); }
		}

		///<summary>
		/// Job Title
		///</summary>
		[ImplementPropertyType("jobTitle")]
		public string JobTitle
		{
			get { return this.GetPropertyValue<string>("jobTitle"); }
		}

		///<summary>
		/// Location
		///</summary>
		[ImplementPropertyType("location")]
		public string Location
		{
			get { return this.GetPropertyValue<string>("location"); }
		}

		///<summary>
		/// Pay Scale
		///</summary>
		[ImplementPropertyType("payScale")]
		public string PayScale
		{
			get { return this.GetPropertyValue<string>("payScale"); }
		}

		///<summary>
		/// Type
		///</summary>
		[ImplementPropertyType("type")]
		public string Type
		{
			get { return this.GetPropertyValue<string>("type"); }
		}
	}
}
