﻿using Merchello.Core;
using Merchello.Core.Events;
using Merchello.Core.Models;
using Merchello.Core.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;

namespace Phoenix.Web.UI.App_Code
{
    /// <summary>
    /// Registers Umbraco event handlers.
    /// </summary>
    public class UmbracoEventHandler : IApplicationEventHandler
    {
        /// <summary>
        /// Handles Umbraco Initialized Event.
        /// </summary>
        /// <param name="umbracoApplication">
        /// The <see cref="UmbracoApplicationBase"/>.
        /// </param>
        /// <param name="applicationContext">
        /// Umbraco <see cref="ApplicationContext"/>.
        /// </param>
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        /// <summary>
        /// Handles Umbraco Starting.
        /// </summary>
        /// <param name="umbracoApplication">
        /// The <see cref="UmbracoApplicationBase"/>.
        /// </param>
        /// <param name="applicationContext">
        /// Umbraco <see cref="ApplicationContext"/>.
        /// </param>
        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {            
            ShipmentService.StatusChanged += ShipmentServiceStatusChanged;
        }

      

        /// <summary>
        /// Example shipment shipped / delivered notification handler
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The <see cref="StatusChangeEventArgs{IShipment}"/>
        /// </param>
        private void ShipmentServiceStatusChanged(IShipmentService sender, StatusChangeEventArgs<IShipment> e)
        {
            var validKeys = new[]
            {
                Merchello.Core.Constants.ShipmentStatus.Shipped
            };

            foreach (var shipment in e.StatusChangedEntities)
            {
                if (!validKeys.Contains(shipment.ShipmentStatus.Key)) continue;

                var contacts = new List<string>();

                contacts.Add(shipment.Email);
                //contacts.Add(ConfigurationManager.AppSettings["OrderEmail"]);

                LogHelper.Info<UmbracoEventHandler>(string.Format("Raising notification trigger for shipment no. {0}", shipment.ShipmentNumber));

                Notification.Trigger("OrderShipped", shipment, contacts, Merchello.Core.Observation.Topic.Notifications);
            }
        }

        /// <summary>
        /// Handles Umbraco Started.
        /// </summary>
        /// <param name="umbracoApplication">
        /// The <see cref="UmbracoApplicationBase"/>.
        /// </param>
        /// <param name="applicationContext">
        /// Umbraco <see cref="ApplicationContext"/>.
        /// </param>
        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }
    }
}
