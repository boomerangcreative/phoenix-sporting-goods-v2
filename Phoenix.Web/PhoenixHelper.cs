﻿using Umbraco.Core.Models;
using umbraco;
using umbraco.cms.businesslogic.web;
using Umbraco.Web;
using System.Web;
using Umbraco.Core;
using System.Linq;

namespace Phoenix.Web
{
    public static class PhoenixHelper
    {
        public static int GetHomeNode()
        {
            int foundNodeID = 0;

            UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            foreach (var rootNode in umbracoHelper.ContentAtRoot())
            {
                int nodeID = rootNode.Id;

                var contentCache = UmbracoContext.Current.ContentCache;
                var services = ApplicationContext.Current.Services;
                var domainService = services.DomainService;
                //Check against all possible domains (as rootNode.Url returns just the most recent domain entry)

                var domains = domainService
                    // First, get each content node with a domain set.
                    .GetAll(true);
                   
                foreach (IDomain currentDomain in domains)
                {
                    if (currentDomain.DomainName.ToLower() == HttpContext.Current.Request.Url.Host.ToLower())
                    {
                        foundNodeID = nodeID;
                    }
                }
            }

            return foundNodeID;

        }

        public static string ResolveTextDropDown(string value)
        {
            return value.Replace(" ", "-").ToLower();

        }
    }
}
