﻿using Merchello.Core.Formatters;
using Merchello.Core.Gateways.Notification;
using Merchello.Core.Observation;
using Merchello.Web.Workflow.Notification.Monitor;
using Phoenix.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Web
{
    [MonitorFor("D5F6A745-4687-4FBF-92D5-AB193354C3BD", typeof(OrderFulfilledTrigger), "Order Fulfilled (Razor)", true)]
    public class RazorOrderFulfilledNotifyMonitor : RazorMonitorBase<IOrderFulfilledNotifyModel>
    {

        public RazorOrderFulfilledNotifyMonitor(INotificationContext notificationContext)
          : base(notificationContext)
        {
        }

        /// <summary>
        /// Trigger call to notifify the monitor of a change
        /// </summary>
        /// <param name="value">
        /// The model to be used by the monitor
        /// </param>
        public override void OnNext(IOrderFulfilledNotifyModel value)
        {
            if (!Messages.Any()) return;

            var formatter = new DefaultFormatter();

            // Add the replaceable patterns 
            formatter.Format(value.Invoice.BillToEmail);

            foreach (var message in Messages)
            {
                if (message.SendToCustomer)
                {
                    message.Recipients = string.Join(";", value.Contacts);
                }

                // send the message
                NotificationContext.Send(message, formatter);
            }
        }
    }
}
