﻿using Merchello.Core.Gateways.Notification.Triggering;
using Merchello.Core.Models;
using Merchello.Core.Observation;
using Phoenix.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;

namespace Phoenix.Web
{
    /// <summary>
    /// A trigger for order fulfilled.
    /// </summary>
    [TriggerFor("OrderFulfilled", Topic.Notifications)]
    public class OrderFulfilledTrigger : NotificationTriggerBase<IOrder, IOrderFulfilledNotifyModel>
    {
        /// <summary>
        /// Value to pass to the notification monitors
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="contacts">
        /// An additional list of contacts
        /// </param>
        protected override void Notify(IOrder model, IEnumerable<string> contacts)
        {
            var invoice = model.Invoice();
            var shipments = new List<IShipment>();
            foreach (var o in invoice.Orders)
            {
                shipments.AddRange(o.Shipments());
            }

            var billingContact = invoice.GetBillingAddress().Email;

            var contactList = contacts?.ToList() ?? new List<string>();

            if (!billingContact.IsNullOrWhiteSpace()) contactList.Add(billingContact);

            var notifyModel = new OrderFulfilledNotifyModel
            {
                Invoice = invoice,
                Order = model,
                Shipments = shipments,
                Contacts = contactList.ToArray()
            };

            NotifyMonitors(notifyModel);
        }
    }
}
