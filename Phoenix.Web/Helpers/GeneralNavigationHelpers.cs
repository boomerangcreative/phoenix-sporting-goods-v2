﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace Phoenix.Web.Helpers
{
    public class GeneralNavigationHelpers
    {
        //--- get content from root store --
        public static IPublishedContent GetStore(dynamic myUmbracoRoot)
        {
            var siteStore = myUmbracoRoot.Where("DocumentTypeAlias = \"ftStore\"").First();
            return siteStore;
        }
        // -- get content from root store basket ----
        public static IPublishedContent GetBasket(dynamic myUmbracoRoot)
        {
            var siteBasket = myUmbracoRoot.Where("DocumentTypeAlias = \"ftStore\"").First();
            var basketNode = siteBasket.Descendants("ftBasket").First();
            return basketNode;
        }

        // -- get content from mini site settings node --
        public static IPublishedContent GetSettings(dynamic myUmbracoRoot)
        {
            var siteSettings = myUmbracoRoot.Where("DocumentTypeAlias = \"Settings\"").First();
            return siteSettings;
        }


    }
}
