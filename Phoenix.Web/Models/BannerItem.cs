﻿namespace Phoenix.Web.Models
{
    public class BannerItemModel
    {
        public string Title { get; set; }

        public string Image { get; set; }

        public string LinkText { get; set; }

        public string Link { get; set; }
    }
}