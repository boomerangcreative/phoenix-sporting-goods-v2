﻿using System.Collections.Generic;
using Merchello.Core.Models;
using Merchello.Core.Models.MonitorModels;

namespace Phoenix.Web.Models
{
    /// <summary>
    /// Represents a notification model for order fulfillment notifications.
    /// </summary>
    public interface IOrderFulfilledNotifyModel : INotifyModel
    {
        /// <summary>
        /// Gets or sets the invoice.
        /// </summary>
        IInvoice Invoice { get; set; }

        /// <summary>
        /// Gets or sets the order.
        /// </summary>
        IOrder Order { get; set; }

        /// <summary>
        /// Gets or sets the shipments.
        /// </summary>
        IEnumerable<IShipment> Shipments { get; set; }
    }
}
