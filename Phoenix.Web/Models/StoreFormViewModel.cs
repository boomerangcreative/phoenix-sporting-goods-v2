﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Phoenix.Web.Models
{
    public class StoreFormViewModel
    {
        [Required(ErrorMessage = "First Name is Required"), Display(Name = "FIRST NAME")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Surname is Required"), Display(Name = "SURNAME")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Email is Required"), Display(Name = "EMAIL ADDRESS")]
        public string Email { get; set; }

        public string ReturnUrl{ get; set; }
    }    
}