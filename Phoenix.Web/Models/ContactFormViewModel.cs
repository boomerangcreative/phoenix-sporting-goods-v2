﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Phoenix.Web.Models
{
    public class ContactFormViewModel
    {
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Full Name *")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email is Required"), Display(Name = "Email Address *")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone Number is Required"), Display(Name = "Telephone *")]
        public string Telephone { get; set; }

        [Required(ErrorMessage = "Message is Required"), Display(Name = "Message *")]
        public string Message { get; set; }
    }
}