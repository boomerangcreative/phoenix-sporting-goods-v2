﻿using Merchello.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phoenix.Web.Models
{
    internal class OrderFulfilledNotifyModel : IOrderFulfilledNotifyModel    
    {
        /// <inheritdoc/>
        public IInvoice Invoice { get; set; }

        /// <inheritdoc/>
        public IOrder Order { get; set; }

        /// <inheritdoc/>
        public IEnumerable<IShipment> Shipments { get; set; }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        public string[] Contacts { get; set; }
    }
}
