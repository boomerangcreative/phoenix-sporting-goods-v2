﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.Web.Models
{
    public class DistributorFormViewModel
    {
        public DistributorFormViewModel()
        {
            AvailableSources = new List<SelectListItem>();
            SelectedSources = new List<string>();
        }

        [Required(ErrorMessage = "Name is Required"), Display(Name = "Full Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email is Required"), Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Address is Required"), Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "No of Years Established")]
        public string YearsEstablished { get; set; }

        [Display(Name = "Main Sports Catered For")]
        public string MainSports { get; set; }
       
        public IList<string> SelectedSources { get; set; }

        [Display(Name = "Do you look after")]
        public IList<SelectListItem> AvailableSources { get; set; }

    }
}