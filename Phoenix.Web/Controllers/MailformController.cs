﻿using Phoenix.Web.Models;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Configuration;
using Pheonix.Web;
using MailChimp.Net.Core;
using System;
using System.Net;
using MailChimp.Net;
using MailChimp.Net.Models;
using System.Web;

namespace Phoenix.Web.Controllers
{
    public class MailFormController : SurfaceController
    {
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> Submit(MailFormViewModel model)
        {
            TempData["success"] = false;

            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            var MailChimpManager = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);

            string MailChimpListId = ConfigurationManager.AppSettings["MailChimpListId"];
            // Use the Status property if updating an existing member


            // Use the Status property if updating an existing member
            var member = new Member
            {
                EmailAddress = model.Email,
                Status = Status.Subscribed,
                EmailType = "html",
                TimestampSignup = DateTime.UtcNow.ToString("s")
            };

            member.MergeFields.Add("FNAME", model.FirstName);
            member.MergeFields.Add("LNAME", model.Surname);
            try
            {

                var result = await MailChimpManager.Members.AddOrUpdateAsync(MailChimpListId, member);
                TempData["Message"] = new MvcHtmlString("<p>Thank you for signing up! </p><p>Please check your inbox for confirmation. (If you do not receive this within the next 24 hours please check your junk folder, thereafter contact <a href=\"mailto:info@phoenixsportinggoods.com\">info@phoenixsportinggoods.com</a> for assistance)</p>");
                TempData["success"] = true;


            }
            catch (MailChimpException mce)
            {
                TempData["success"] = false;

                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }

            return RedirectToCurrentUmbracoPage();

        }

        //[HttpPost]
        //public ActionResult SubmitMail(MailingListFormViewModel model)
        //{
        //    TempData["success"] = false;

        //    if (!ModelState.IsValid)
        //        return CurrentUmbracoPage();

        //    // Use the Status property if updating an existing member

        //    try
        //    {
        //        var success = MailChimpHelper.SendMailingListAsync(model.Email, model.FirstName, model.Surname);
        //        success.Wait();

        //        var isValid = success.Result;
        //        if (isValid)
        //        {
        //            TempData["Message"] = "Your email has been added to the mailing list.";
        //            TempData["success"] = true;
        //        }
        //        else
        //        {
        //            TempData["success"] = false;
        //        }


        //    }
        //    catch (MailChimpException mce)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
        //    }

        //    return RedirectToCurrentUmbracoPage();

        //}
    }
}