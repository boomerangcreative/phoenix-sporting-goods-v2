﻿using Phoenix.Web.Models;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Collections.Generic;
using Pheonix.Web;

namespace Phoenix.Web.Controllers
{
    public class DistributorFormController : SurfaceController
    {
        [HttpGet]
        public ActionResult Index()
        {
            var model = new DistributorFormViewModel
            {
                AvailableSources = GetTypes()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(DistributorFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                var types = string.Join(",", model.SelectedSources);

                // Save data to database, and redirect to Success page.

                return RedirectToAction("Success");
            }
            model.AvailableSources = GetTypes();
            return View(model);
        }

        public ActionResult Success()
        {
            return View();
        }



        [HttpPost]
        public ActionResult Submit(DistributorFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }
                

            /// Work with form data here

            var success = EmailHelper.SendDistributorFormEmail(model.Name, model.Email, model.Address, model.Website, model.YearsEstablished,model.MainSports, model.SelectedSources);

            TempData["Message"] = "Your email has been sent";

            return RedirectToCurrentUmbracoPage();
        }

        private IList<SelectListItem> GetTypes()
        {
            return new List<SelectListItem>
            {
                new SelectListItem {Text = "Education", Value = "Education"},
                new SelectListItem {Text = "Clubs", Value = "Clubs"},
                new SelectListItem {Text = "Retail", Value = "Retail"}
            };
        }
    }
}