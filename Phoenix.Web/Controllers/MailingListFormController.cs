﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Phoenix.Web.Models;
using MailChimp.Net;
using MailChimp.Net.Core;
using MailChimp.Net.Models;
using System.Configuration;
using System.Net;
using Umbraco.Web.Mvc;
using Pheonix.Web;
namespace Phoenix.Web.Controllers
{
    public class MailingListFormController : SurfaceController
    {
        [HttpPost]
        public ActionResult Submit(MailingListFormViewModel model)
        {
            TempData["success"] = false;

            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // Use the Status property if updating an existing member
            
            try
            {
                var success = MailChimpHelper.SendMailingListAsync(model.Email, model.FirstName, model.Surname);
                success.Wait();

                var isValid = success.Result;
                if (isValid)
                {
                    TempData["Message"] = "Your email has been added to the mailing list.";
                    TempData["success"] = true;
                }
                else
                {
                    TempData["success"] = false;
                }
                

            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }

            return RedirectToCurrentUmbracoPage();

        }


    }
}
