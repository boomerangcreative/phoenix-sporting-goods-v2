﻿using Phoenix.Web.Models;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Configuration;
using Pheonix.Web.UI;
using MailChimp.Net.Core;
using System;
using System.Net;


namespace Phoenix.Web.Controllers
{
    public class StoreFormController : SurfaceController
    {
        [HttpPost]
        public ActionResult Submit(MailingFormViewModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            /// Work with form data here

            var success = EmailHelper.SendContactFormEmail(model.Email, ConfigurationManager.AppSettings["fromEmail"], model.Name, ConfigurationManager.AppSettings["ContactFormSubject"], model.Telephone, model.Message);

            TempData["Message"] = "Your email has been sent";

            return RedirectToCurrentUmbracoPage();
        }

        [HttpPost]
        public ActionResult SubmitMail(MailingFormViewModel model)
        {
            TempData["success"] = false;

            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // Use the Status property if updating an existing member

            try
            {
                var success = MailChimpHelper.SendMailingListAsync(model.Email, model.FirstName, model.Surname);
                success.Wait();

                var isValid = success.Result;
                if (isValid)
                {
                    TempData["Message"] = "Your email has been added to the mailing list.";
                    TempData["success"] = true;
                }
                else
                {
                    TempData["success"] = false;
                }


            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }

            return RedirectToCurrentUmbracoPage();

        }
    }
}
}