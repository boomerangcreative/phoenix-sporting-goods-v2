﻿using System;
using System.Web.Mvc;

using Merchello.Core;
using Merchello.Web.Controllers;
using Merchello.Web.Factories;
using Merchello.Web.Store.Factories;
using Merchello.Web.Store.Models;
using Merchello.Web.Store.Models.Async;
using Merchello.Web.Models.ContentEditing;
using System.Linq;
using System.Collections.Generic;

using Umbraco.Web.Mvc;
using Newtonsoft.Json;

namespace Merchello.Web.Store.Controllers
{
    [PluginController("Phoenix")]
    public class PhoenixBasketController : BasketControllerBase<StoreBasketModel, StoreLineItemModel, StoreAddItemModel>
    {
        /// <summary>
        /// The factory responsible for building <see cref="ExtendedDataCollection"/>s when adding items to the basket.
        /// </summary>
        private readonly BasketItemExtendedDataFactory<StoreAddItemModel> _addItemExtendedDataFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreBasketController"/> class.
        /// </summary>
        /// <remarks>
        /// This constructor allows you to inject your custom model factory overrides so that you can
        /// extended the various model interfaces with site specific models.  In this case, we have overridden 
        /// the BasketModelFactory and the AddItemModelFactory.  The BasketItemExtendedDataFactory has not been overridden.
        /// 
        /// Views rendered by this controller are placed in "/Views/QuickMartBasket/" and correspond to the method name.  
        /// 
        /// e.g.  the "AddToBasketForm" corresponds the the AddToBasketForm method in BasketControllerBase. 
        /// 
        /// This is just an generic MVC pattern and nothing to do with Umbraco
        /// </remarks>
        public PhoenixBasketController()
            : this(new BasketItemExtendedDataFactory<StoreAddItemModel>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreBasketController"/> class.
        /// </summary>
        /// <param name="addItemExtendedDataFactory">
        /// The <see cref="BasketItemExtendedDataFactory{StoreAddItemModel}"/>.
        /// </param>
        public PhoenixBasketController(BasketItemExtendedDataFactory<StoreAddItemModel> addItemExtendedDataFactory)
            : base(addItemExtendedDataFactory, new AddItemModelFactory(), new BasketModelFactory())
        {
        }

        /// <summary>
        /// Handles the successful basket update.
        /// </summary>
        /// <param name="model">
        /// The <see cref="StoreBasketModel"/>.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        /// <remarks>
        /// Customization of the handling of an add item success
        /// </remarks>
        protected override ActionResult HandleAddItemSuccess(StoreAddItemModel model)
        {
            if (Request.IsAjaxRequest())
            {
                // Construct the response object to return
                var resp = new AddItemAsyncResponse
                {
                    Success = true,
                    ItemCount = this.GetBasketItemCountForDisplay()
                };

                return this.Json(resp);
            }

            return base.HandleAddItemSuccess(model);
        }

        /// <summary>
        /// Handles an add item operation exception.
        /// </summary>
        /// <param name="model">
        /// The <see cref="StoreAddItemModel"/>.
        /// </param>
        /// <param name="ex">
        /// The <see cref="Exception"/>.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        protected override ActionResult HandleAddItemException(StoreAddItemModel model, Exception ex)
        {
            if (Request.IsAjaxRequest())
            {
                // in case of Async call we need to construct the response
                var resp = new AddItemAsyncResponse { Success = false, Messages = { ex.Message } };
                return this.Json(resp);
            }

            return base.HandleAddItemException(model, ex);
        }

        /// <summary>
        /// Responsible for updating the quantities of items in the basket
        /// </summary>
        /// <param name="model">The <see cref="IBasketModel{TBasketItemModel}"/></param>
        /// <returns>Redirects to the current Umbraco page (generally the basket page)</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult AddBasketItem(StoreAddItemModel model)
        {

            // Instantiating the ExtendedDataCollection in this manner allows for additional values 
            // to be added in the factory OnCreate override.
            // e.g. if you need to store custom extended data values, create your own factory
            // inheriting from BasketItemExtendedDataFactory and override the "OnCreate" method to store
            // any addition values you have added to the model
            var extendedData = this._addItemExtendedDataFactory.Create(model);

            // We've added some data modifiers that can handle such things as including taxes in product
            // pricing.  The data modifiers can either get executed when the item is added to the basket or
            // as a result from a MerchelloHelper query - you just don't want them to execute twice.

            // In this case we want to get the product without any data modification
            var merchello = new MerchelloHelper(false);

            var product = merchello.Query.Product.GetByKey(model.ProductKey);

            // ensure the quantity on the model
            var quantity = model.Quantity <= 0 ? 1 : model.Quantity;

            // In the event the product has options we want to add the "variant" to the basket.
            // -- If a product that has variants is defined, the FIRST variant will be added to the cart. 
            // -- This was done so that we did not have to throw an error since the Master variant is no
            // -- longer valid for sale.
            if (model.OptionChoices != null && model.OptionChoices.Any())
            {
                var variant = product.GetProductVariantDisplayWithAttributes(model.OptionChoices);

                // Log the option choice for this variant in the extend data collection
                var choiceExplainations = new Dictionary<string, string>();
                foreach (var choice in variant.Attributes)
                {
                    var option = product.ProductOptions.FirstOrDefault(x => x.Key == choice.OptionKey);
                    if (option != null)
                    {
                        choiceExplainations.Add(option.Name, choice.Name);
                    }
                }

                // store the choice explainations in the extended data collection
                extendedData.SetValue(Core.Constants.ExtendedDataKeys.BasketItemCustomerChoice, JsonConvert.SerializeObject(choiceExplainations));

                this.Basket.AddItem(variant, variant.Name, quantity, extendedData);
            }
            else
            {
                this.Basket.AddItem(product, product.Name, quantity, extendedData);
            }

            this.Basket.Save();

            // If this request is not an AJAX request return the redirect
            return this.HandleAddItemSuccess(model);
        }



    }
}