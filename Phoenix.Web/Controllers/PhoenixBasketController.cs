﻿using Merchello.Web.Controllers;
using Merchello.Web.Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Phoenix.Web.Controllers
{
    public class PhoenixBasketController : BasketControllerBase<StoreBasketModel, StoreLineItemModel, StoreAddItemModel>
    {
        /// <summary>
        /// Responsible for adding a product to the basket.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult AddBasketItem(StoreAddItemModel model)
        {
            return View();
        }
    }
}