﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Mandrill;

using Mandrill.Models;
using Mandrill.Requests.Messages;
using System.Configuration;
using System.Web.UI.WebControls;
using Merchello.FastTrack.Models.Membership;
using MailChimp.Net;
using MailChimp.Net.Models;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;

namespace Pheonix.Web
{
    public static class MailChimpHelper
    {
        private static MailChimpManager MailChimpManager = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);

        private static string MailChimpListId = ConfigurationManager.AppSettings["MailChimpListId"];

        public static async Task<bool> SendMailingListAsync(string email, string firstName, string surname)
        {

            // Use the Status property if updating an existing member
            var member = new Member
            {
                EmailAddress = email,
                Status = Status.Subscribed,
                EmailType = "html",
                TimestampSignup = DateTime.UtcNow.ToString("s")
            };

            member.MergeFields.Add("FNAME", firstName);
            member.MergeFields.Add("LNAME", surname);

            try
            {
                var result = await MailChimpManager.Members.AddOrUpdateAsync(MailChimpListId, member);
                return true;
            }
            catch
            {
                return false;
            }

        }
    }

    public class EmailHelper
    {
        public static string SendContactFormEmail(string fromEmail, string toEmail, string name, string subject, string telephone, string body)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromEmail);

            message.To.Add(toEmail);

            //foreach(var ccmail in ccEmails)
            //    message.CC.Add(new MailAddress(ccmail));

            message.Subject = subject;
            message.Body = name + "\r\n" + telephone + "\r\n" + body;

            SmtpClient mailer = new SmtpClient();
            //SMTP Details setup in Web.config          

            try
            {
                mailer.Send(message);
                return "success";
            }
            catch (Exception ex)
            {
                return string.Format("Exception caught in CreateTestMessage1(): {0}", ex.ToString());
            }
        }

        public static string SendDistributorFormEmail(string name, string email, string address, string website, string years, string sports, IList<string> types)
        {
            MailDefinition md = new MailDefinition();            
            md.From = email;
            md.IsBodyHtml = true;
            md.Subject = "Distributor Enquiry Form";
            //message.To.Add(new MailAddress(ConfigurationManager.AppSettings["FromEmail"]));

            var listTypes = new StringBuilder();

            foreach(var type in types)
            {
                listTypes.Append(type + ",");
            }

            ListDictionary replacements = new ListDictionary();
            replacements.Add("{name}", name);
            replacements.Add("{email}", email);
            replacements.Add("{address}", address);
            replacements.Add("{website}", website);
            replacements.Add("{sports}", sports);
            replacements.Add("{types}", listTypes.ToString());

            string body = "<div>Name : {name} <br/>Email: {email} <br/> Address: {address} <br/> Website: {website} <br/> Established Years: {years} <br/> Main sports: {sports}</div>";
            var msg = md.CreateMailMessage(ConfigurationManager.AppSettings["FromEmail"], replacements, body, new System.Web.UI.Control());

            SmtpClient mailer = new SmtpClient();
            //SMTP Details setup in Web.config          

            try
            {
                mailer.Send(msg);
                return "success";
            }
            catch (Exception ex)
            {
                return string.Format("Exception caught in CreateTestMessage1(): {0}", ex.ToString());
            }
        }
    }

    public static class MandrillHelper
    {
        public static async Task<List<EmailResult>> SendConfirmationEmail(string customerName, string customerEmail, string fullMember, string wow, string activation, string orderTotal, string username, string fullMemAden, string invoiceID)
        {
            var api = new MandrillApi(ConfigurationManager.AppSettings["MandrillAPIkey"]);

            var mandrillRecipients = new List<EmailAddress>();
            mandrillRecipients.Add(new EmailAddress(customerEmail));

            var email = Setup(mandrillRecipients);

            email.AddGlobalVariable("CUSTOMERNAME", GetName(customerName, customerEmail).ToUpper());
            email.AddGlobalVariable("FULLMEMBERSHIP", fullMember);
            email.AddGlobalVariable("WORDSOFWISDOM", wow);
            email.AddGlobalVariable("ACTIVATION", activation);
            email.AddGlobalVariable("ORDERTOTAL", orderTotal);
            email.AddGlobalVariable("USERNAME", username);
            email.AddGlobalVariable("FULLMEMADEN", fullMemAden);
            email.AddGlobalVariable("OrderID", invoiceID);

            var request = new SendMessageTemplateRequest(email, "confirmation-email", null);

            List<EmailResult> result = await api.SendMessageTemplate(new SendMessageTemplateRequest(email, "confirmation-email"));

            return result;
        }

        public static string SendSMTPEmail(string fromEmail, string toEmail, string subject, string body)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromEmail);

            message.To.Add(toEmail);

            //foreach(var ccmail in ccEmails)
            //    message.CC.Add(new MailAddress(ccmail));

            message.Subject = subject;
            message.Body = body;

            SmtpClient mailer = new SmtpClient();
            //SMTP Details setup in Web.config

            mailer.Host = ConfigurationManager.AppSettings["MandrillHost"];
            mailer.Port = int.Parse(ConfigurationManager.AppSettings["MandrillPort"]);
            mailer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MandrillUsername"], ConfigurationManager.AppSettings["MandrillAPIkey"]);

            try
            {
                mailer.Send(message);
                return "success";
            }
            catch (Exception ex)
            {
                return string.Format("Exception caught in CreateTestMessage1(): {0}", ex.ToString());
            }
        }

        public static string SendFailedNotificationSMTPEmailOne(string number, string invoiceKey, string username, string installationId, string transStatus, string transTime, string futurePayId, string futurePayStatusChange, string email, string address, string description, string exception)
        {
            return SendFailedNotificationSMTPEmail(invoiceKey, username, installationId, transStatus, transTime, futurePayId, futurePayStatusChange, email, address, description, exception);
        }

        public static string SendFailedNotificationSMTPEmailTwo(string number, string invoiceKey, string username, string installationId, string transStatus, string transTime, string futurePayId, string futurePayStatusChange, string email, string address, string description, string exception)
        {
            return SendFailedNotificationSMTPEmail(invoiceKey, username, installationId, transStatus, transTime, futurePayId, futurePayStatusChange, email, address, description, exception);
        }

        public static string SendFailedNotificationSMTPEmail(string invoiceKey, string username, string installationId, string transStatus, string transTime, string futurePayId, string futurePayStatusChange, string email, string address, string description, string exception)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["MandrillFromEmail"]);

            message.To.Add(ConfigurationManager.AppSettings["MandrillTechSupportEmail"]);

            var body = new StringBuilder();

            body.Append("<p>Invoice Key: " + invoiceKey + "</p>");
            body.Append("<p>Username: " + username + "</p>");
            body.Append("<p>InstallationId: " + installationId + "</p>");
            body.Append("<p>Transaction Status: " + transStatus + "</p>");
            body.Append("<p>TransTime: " + transTime + "</p>");
            body.Append("<p>FuturePayId: " + futurePayId + "</p>");
            body.Append("<p>Futurepay StatusChange: " + futurePayStatusChange + "</p>");
            body.Append("<p>Email: " + email + "</p>");
            body.Append("<p>Address: " + address + "</p>");
            body.Append("<p>Description: " + description + "</p>");
            body.Append("<p>Exception: " + exception + "</p>");

            message.Subject = "Something went wrong";
            message.Body = body.ToString();

            SmtpClient mailer = new SmtpClient();
            //SMTP Details setup in Web.config

            mailer.Host = ConfigurationManager.AppSettings["MandrillHost"];
            mailer.Port = int.Parse(ConfigurationManager.AppSettings["MandrillPort"]);
            mailer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MandrillUsername"], ConfigurationManager.AppSettings["MandrillAPIkey"]);

            try
            {
                mailer.Send(message);
                return "success";
            }
            catch (Exception ex)
            {
                return string.Format("Exception caught in CreateTestMessage1(): {0}", ex.ToString());
            }
        }      

        public static async Task<List<EmailResult>> SendForgottenPasswordEmail(string customerName, string customerEmail, string password)
        {
            var api = new MandrillApi(ConfigurationManager.AppSettings["MandrillAPIkey"]);

            var mandrillRecipients = new List<EmailAddress>();
            mandrillRecipients.Add(new EmailAddress(customerEmail));

            var email = Setup(mandrillRecipients);

            email.AddGlobalVariable("CUSTOMERNAME", GetName(customerName, customerEmail).ToUpper());
            email.AddGlobalVariable("PASSWORD", password);

            var request = new SendMessageTemplateRequest(email, "forgotten-password", null);

            List<EmailResult> result = await api.SendMessageTemplate(new SendMessageTemplateRequest(email, "forgotten-password"));

            return result;
        }

        public static async Task<List<EmailResult>> SendFailedPaymentEmail(string customerName, string customerEmail)
        {
            var api = new MandrillApi(ConfigurationManager.AppSettings["MandrillAPIkey"]);

            var mandrillRecipients = new List<EmailAddress>();
            mandrillRecipients.Add(new EmailAddress(customerEmail));

            var email = Setup(mandrillRecipients);

            email.AddGlobalVariable("CUSTOMERNAME", GetName(customerName, customerEmail).ToUpper());
            email.AddGlobalVariable("USERNAME", customerEmail);

            var request = new SendMessageTemplateRequest(email, "forgotten-password", null);

            List<EmailResult> result = await api.SendMessageTemplate(new SendMessageTemplateRequest(email, "forgotten-password"));

            return result;
        }

        public static string SendEmailRegisterMember(NewMemberModel model)
        {
            string result = "";
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.To.Add(model.Email);
                message.Subject = "Account Registration confirm";
                message.From = new System.Net.Mail.MailAddress("info@xyzc.com");
                message.Body = string.Format("Hi {0}, <br/> registration confirmed. login {1} - password {2} .<br/>Thanks.", model.FirstName, model.Email, model.Password);
                message.IsBodyHtml = true;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = "smtp.mandrillapp.com";
                smtp.Port = 587;

                smtp.Send(message);
                result = "Ok. Sent to " + model.Email;
            }
            catch (Exception ex)
            {
                result = "Error: " + ex.Message;
            }

            return result;

        }

        private static string GetName(string name, string email)
        {
            if (string.IsNullOrEmpty(name))
            {
                return email;
            }
            else
            {
                return name;
            }
        }

        private static EmailMessage Setup(List<EmailAddress> recipients)
        {
            var email = new EmailMessage
            {
                To = recipients,
                FromEmail = ConfigurationManager.AppSettings["MandrillFromEmail"],
                FromName = ConfigurationManager.AppSettings["MandrillFromName"],
            };

            return email;
        }
    }


}
